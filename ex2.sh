#! /bin/bash
#Quadratic Equation

function input(){
	echo "-------------Input---------------"
	echo -n "a: "
	read a
	echo -n "b: "
	read b
	echo -n "c: "
	read c
}

input 
while echo $a $b $c | egrep -v "^[1-9]" > /dev/null 2>&1
do
	echo "status: -1"
	input
done

delta=$((b*b-4*a*c))
if [ $delta -gt 0 ]
then
	echo "status: 2"
	echo -n "x1 = "
    echo -e "scale=3\n0.5*(-($b)+sqrt($delta))/($a)" | bc
    echo -n "x2 = "
    echo -e "scale=3\n0.5*(-($b)-sqrt($delta))/($a)" | bc
elif [ $delta -eq 0 ]
then
	echo "status: 1"
	echo -n "x = "
	echo $(((-b)/(2*a)))
else
	echo "status: 0"
fi
