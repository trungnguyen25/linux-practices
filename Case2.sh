#! /bin/bash
#Case2.sh

echo "Input: "
read num

while echo $num | egrep -v "^[1-9]" > /dev/null 2>&1
do
	echo "You must input a number"
	echo "Input: "
	read num
done

for((n=1;n<=num;n++));
do
	for((m=1;m<=num;m++));
	do
		echo -n "$m "
	done
	echo ""
done
