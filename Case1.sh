#! /bin/bash
#Case1.sh

echo "Input: "
read num

while echo $num | egrep -v "^[1-9]" > /dev/null 2>&1
do
	echo "You must input a number"
	echo "Input: "
	read num
done

for((line=1;line<=num;line++));
do
	for((colum=1;colum<=line;colum++));
	do
		echo -n "*"
	done
	echo ""
done
